#include <iostream>
#include <cmath>
#include <tuple>
#include <SDL.h>

#define WIDTH  640
#define HEIGHT 480

void write(unsigned int *framebuffer, int x, int y, int value)
{
    framebuffer[x+(y*WIDTH)] = value;
}

void shiftAndWrite(unsigned int *framebuffer, int x, int y, int value)
{
    //std::cout << "writing: " << value << " to: " << x << ", " << y << std::endl;
    
    value = (value << 8) | value;
    value = (value << 16) | value;
    
    framebuffer[x+(y*WIDTH)] = value;
}

int readAndUnshift(unsigned int *framebuffer, int x, int y)
{
    auto value = framebuffer[x+(y*WIDTH)];
    
    value = 0xFF & value;
    
    return value;
}

int read(unsigned int *framebuffer, int x, int y)
{
    return framebuffer[x+(y*WIDTH)];
}

std::tuple<double, double> adjustValue(std::tuple<double,double> t)
{
    auto x = std::get<0>(t);
    auto y = std::get<1>(t);
    
    if (x > 0)
    {
        x -= 0.5;
        x = floor(x);
    }
    if (y > 0)
    {
        y -= 0.5;
        y = floor(y);
    }
    
    return std::make_tuple(x, y);
}

void generateDiamondSquareNoise(unsigned int *framebuffer, double startX, double startY, double width, double height)
{
    if ((int)width <= 1 && (int)height <= 1)
    {
        return;
    }
    
    auto posCenter  = std::make_tuple(startX+(width/2), startY+(height/2)); // 1, 1 = 1.5, 1.5
    auto posUp      = std::make_tuple(startX+(width/2), startY);            // 1, 0 = 1.5, 0
    auto posLeft    = std::make_tuple(startX,           startY+(height/2)); // 0, 1 = 0  , 1.5
    auto posRight   = std::make_tuple(startX+width,     startY+(height/2)); // 2, 1 = 3  , 1.5
    auto posDown    = std::make_tuple(startX+(width/2), startY+height);     // 1, 2 = 1.5, 3
    
    posCenter   = adjustValue(posCenter);
    posUp       = adjustValue(posUp);
    posLeft     = adjustValue(posLeft);
    posRight    = adjustValue(posRight);
    posDown     = adjustValue(posDown);
    
    auto v0 = readAndUnshift(framebuffer, startX,       startY);
    auto v1 = readAndUnshift(framebuffer, startX+width, startY);
    auto v2 = readAndUnshift(framebuffer, startX,       startY+height);
    auto v3 = readAndUnshift(framebuffer, startX+width, startY+height);
    
    auto vCenter    = (v0+v1+v2+v3)/4;
    auto vLeft      = (v0+v2)/2;
    auto vRight     = (v1+v3)/2;
    auto vUp        = (v0+v1)/2;
    auto vDown      = (v2+v3)/2;
    
    shiftAndWrite(framebuffer, std::get<0>(posUp),      std::get<1>(posUp),     vUp);
    shiftAndWrite(framebuffer, std::get<0>(posLeft),    std::get<1>(posLeft),   vLeft);
    shiftAndWrite(framebuffer, std::get<0>(posRight),   std::get<1>(posRight),  vRight);
    shiftAndWrite(framebuffer, std::get<0>(posDown),    std::get<1>(posDown),   vDown);
    shiftAndWrite(framebuffer, std::get<0>(posCenter),  std::get<1>(posCenter), vCenter);
    
    generateDiamondSquareNoise(framebuffer, startX, startY,                         width/2, height/2);
    generateDiamondSquareNoise(framebuffer, startX+(width/2), startY,               width/2, height/2);
    generateDiamondSquareNoise(framebuffer, startX, startY+(height/2),              width/2, height/2);
    generateDiamondSquareNoise(framebuffer, startX+(width/2), startY+(height/2),    width/2, height/2);
}


void fill(unsigned int *framebuffer, int startX, int startY, int width, int height)
{
    if (width == 1 && height == 1)
    {
        shiftAndWrite(framebuffer, startX, startY, 0xFF);
        shiftAndWrite(framebuffer, startX+1, startY, 0xFF);
        shiftAndWrite(framebuffer, startX, startY+1, 0xFF);
        shiftAndWrite(framebuffer, startX+1, startY+1, 0xFF);
        
        return;
    }
    
    // 0,0 -> 1,1
    fill(framebuffer, startX,             startY,             width/2, height/2); // upper left
    fill(framebuffer, startX+(width/2),   startY,             width/2, height/2); // upper right
    fill(framebuffer, startX,             startY+(height/2),  width/2, height/2); // lower left
    fill(framebuffer, startX+(width/2),   startY+(height/2),  width/2, height/2); // lower right
}

std::tuple<int, int> findMissing(unsigned int *framebuffer, int width, int height)
{
    for (auto x = 0; x < width; x++)
    {
        for (auto y = 0; y < width; y++)
        {
            if (read(framebuffer, x, y) == 0)
            {
                return std::make_tuple(x, y);
            }
        }
    }
    
    return std::make_tuple(-1, -1);
}

int main(int argc, const char * argv[])
{
    SDL_Renderer *ren;
    SDL_Window *win;
    
    unsigned int *framebuffer = new unsigned int[(WIDTH+1)*(WIDTH+1)];
    
    memset(framebuffer, 0x00, WIDTH*WIDTH*sizeof(unsigned int));
    
    SDL_Init(SDL_INIT_VIDEO);
    
    SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &win, &ren);
    
    SDL_Event event;
    
    SDL_Texture* tex = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGB444, SDL_TEXTUREACCESS_STATIC, WIDTH, WIDTH);
    
    unsigned int rnd0 = rand()%255;
    unsigned int rnd1 = rand()%255;
    unsigned int rnd2 = rand()%255;
    unsigned int rnd3 = rand()%255;
    
    /*
    rnd0 = 0;
    rnd1 = 2;
    rnd2 = 4;
    rnd3 = 8;
    */
    /*
    shiftAndWrite(framebuffer, 0, 0, rnd0);
    shiftAndWrite(framebuffer, 3, 0, rnd1);
    shiftAndWrite(framebuffer, 0, 3, rnd2);
    shiftAndWrite(framebuffer, 3, 3, rnd3);
    */
    
    auto offset = 10;
    
    shiftAndWrite(framebuffer, 0, 0, rnd0);
    shiftAndWrite(framebuffer, offset, 0, rnd1);
    shiftAndWrite(framebuffer, 0, offset, rnd2);
    shiftAndWrite(framebuffer, offset, offset, rnd3);
    
    generateDiamondSquareNoise(framebuffer, 0, 0, offset, offset);
    //fill(framebuffer, 0, 0, WIDTH, WIDTH);
    
    for (auto x = 0; x < 3; x++)
    {
        for (auto y = 0; y < 3; y++)
        {
            std::cout << readAndUnshift(framebuffer, x, y) << " ";
        }
        
        std::cout << std::endl;
    }
    
    //auto missingCoords = findMissing(framebuffer, WIDTH, WIDTH);
    
    while (true)
    {
        if (SDL_PollEvent(&event))
        {
            if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE)
            {
                return 0;
            }
        }
        
        SDL_UpdateTexture(tex, NULL, framebuffer, WIDTH * sizeof(unsigned int));
        
        SDL_RenderCopy(ren, tex, NULL, NULL);
        SDL_RenderPresent(ren);
    }
    
    delete[] framebuffer;
    
    SDL_DestroyTexture(tex);
    
    SDL_Quit();
    
    return 0;
}

